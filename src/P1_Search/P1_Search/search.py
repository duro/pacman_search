# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem: SearchProblem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.
    """
    from util import Stack

    # Creamos una pila para manejar los estados a expandir
    stack = Stack()
    # Creamos un conjunto para evitar duplicados
    visited = set()

    # Inicializamos la pila con el estado inicial y la lista de acciones vacía
    initial_state = problem.getStartState()
    stack.push((initial_state, []))

    # Iteramos sobre la lista hasta que se vacíe o encontremos un camino
    while not stack.isEmpty():
        # Sacamos la tupla de path/estado y extraemos los miembros
        state, path = stack.pop()

        # Verificamos si es un estado objetivo
        if problem.isGoalState(state):
            return path

        # Si no ha sido visitado, lo marcamos y expandimos sus sucesores
        if state not in visited:
            visited.add(state)
            successors = problem.getSuccessors(state)
            for child, direction, _ in successors:
                # Agregamos el sucesor a la pila con el nuevo camino
                stack.push((child, path + [direction]))

    return None


def breadthFirstSearch(problem: SearchProblem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    from util import Queue
    fringe = Queue()  # Fringe para manejar qué estados expandir
    fringe.push((problem.getStartState(), []))  # Inicializamos con estado inicial y lista vacía
    visited = []  # Lista para verificar si el estado ya ha sido visitado

    while not fringe.isEmpty():
        state, path = fringe.pop()  # Sacamos la tupla de path/estado y descomponemos

        if problem.isGoalState(state):
            return path

        if state not in visited:
            visited.append(state)
            successors = problem.getSuccessors(state)
            for child, direction, _ in successors:
                # Agregamos el sucesor a la pila con el nuevo camino
                fringe.push((child, path + [direction]))
        
    return None 

def uniformCostSearch(problem: SearchProblem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"
    # Utilizamos una cola con prioridad para ordenar los nodos según el costo acumulado
    pq = util.PriorityQueue()
    
    # Conjunto para almacenar los nodos visitados
    visited = []
    
    # Inicializamos la cola con el estado inicial y un costo acumulado de 0
    pq.push((problem.getStartState(), []), 0)
    
    while not pq.isEmpty():
        state, path = pq.pop()
        
        # Verificamos si es un estado objetivo
        if problem.isGoalState(state):
            return path
        
        # Si el estado no ha sido visitado, lo marcamos y expandimos sus sucesores
        if state not in visited:
            visited.append(state)
            successors = problem.getSuccessors(state)
            for child, direction, cost in successors:
                # Agregamos el sucesor a la cola con prioridad con el nuevo camino y costo acumulado
                pq.push((child, path + [direction]), problem.getCostOfActions(path + [direction]))
    
    return None

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem: SearchProblem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    # Utilizamos una cola con prioridad para ordenar los nodos según el costo acumulado
    pq = util.PriorityQueue()
    
    # Conjunto para almacenar los nodos visitados
    visited = []
    
    # Inicializamos la cola con el estado inicial y un costo acumulado de 0
    pq.push((problem.getStartState(), []), 0)
    
    while not pq.isEmpty():
        state, path = pq.pop()
        
        # Verificamos si es un estado objetivo
        if problem.isGoalState(state):
            return path
        
        # Si el estado no ha sido visitado, lo marcamos y expandimos sus sucesores
        if state not in visited:
            visited.append(state)
            successors = problem.getSuccessors(state)
            for child, direction, cost in successors:
                # Agregamos el sucesor a la cola con prioridad con el nuevo camino y costo acumulado
                pq.push((child, path + [direction]), problem.getCostOfActions(path + [direction]) + heuristic(child,problem))
    
    return None


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
